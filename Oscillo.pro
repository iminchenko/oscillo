#-------------------------------------------------
#
# Project created by QtCreator 2017-02-16T17:26:47
#
#-------------------------------------------------

QT       += core gui
QT += charts
QT += datavisualization

CONFIG += c++14

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Oscillo
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    function.cpp \
    mathdefines.cpp \
    functionbuilder.cpp \
    inputwidget.cpp \
    plotswidget.cpp

HEADERS  += mainwindow.h \
    function.h \
    mathdefines.h \
    functionbuilder.h \
    inputwidget.h \
    plotswidget.h \
    modes.h

FORMS    += mainwindow.ui

win32: LIBS += -lopengl32
