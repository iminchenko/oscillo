// Copyright 2017 <Ilia Minchenko>
#include <math.h>

#include "function.h"
#include "mathdefines.h"

using namespace MathConst;
using std::list;


double zeroFunction() {
    return 0;
}

double zeroFunction(double a, const std::list<double> &lst) {
    return 0;
}

double zeroFunction(double a, double b, const std::list<double> &lst) {
    return 0;
}

double zeroFunction(double a, double b, double c,
                    const std::list<double> &lst) {
    return 0;
}

namespace classic {
namespace prt {
#define init \
auto iter = initial.cbegin();\
double m, xc, pc, dx, dp, r;\
m = *iter;\
xc = *(++iter);\
pc = *(++iter);\
dx = *(++iter);\
dp = *(++iter);\
r = *(++iter);

double probabilityDistribution(double x, double p, double t, double m,
                           double xc, double pc, double dx, double dp, double r) {
    return 1./(2.*Pi*dx*dp*sqrt(1. - sqr(r)))*
            exp(-1./(2.*(1. - sqr(r)))*(sqr(p - pc)/sqr(dp) + sqr(x - p/m*t - xc)/sqr(dx) -
                2.*r*(x - p/m*t - xc)*(p - pc)/dx/dp));
}

double probabilityDistribution(double x, double p, double t,
                           const std::list<double> &initial) {
    init

    return probabilityDistribution(x, p, t, m, xc, pc, dx, dp, r);
}

double probabilityDistributionMax(const std::list<double> &initial) {
    init

    return probabilityDistribution(xc, pc, 0, m, xc, pc, dx, dp, r);
}

double probabilityDistributionX(double x, double t, double m,
                                double xc, double pc, double dx, double dp, double r) {
    return ((exp(-(sqr(pc*t + m*(-x + xc))/(
      2*(sqr(dx)*sqr(m) + 2*dp*dx*m*r*t + sqr(dp)*sqr(t))))))*m )/(sqrt(2*Pi)*sqrt(
     sqr(dx)*sqr(m) + 2*dp*dx*m*r*t + sqr(dp)*sqr(t)));
}

double probabilityDistributionX(double x, double t,
                                const std::list<double> &initial){
    init

    return probabilityDistributionX(x, t, m, xc, pc, dx, dp, r);
}

double probabilityDistributionP(double p, double t, double m,
                                double xc, double pc, double dx, double dp, double r) {
    return exp(-(sqr(p - pc)/(2*sqr(dp))))/(dp*sqrt(2*Pi));
}

double probabilityDistributionP(double p, double t,
                                const std::list<double> &initial) {
    init

    return probabilityDistributionP(p, t, m, xc, pc, dx, dp, r);
}

double averageX(double t, double m, double xc, double pc,
                double dx, double dp, double r) {
    return xc+pc/m*t;
}

double averageX(double t, const std::list<double> &initial) {
    init

    return averageX(t, m, xc, pc, dx, dp, r);
}

double averageP(double t, double m, double xc, double pc,
                double dx, double dp, double r) {
    return pc;
}

double averageP(double t, const std::list<double> &initial) {
    init

    return averageP(t, m, xc, pc, dx, dp, r);
}

} // namespace prt

namespace osc {

#define init\
    auto iter = initial.cbegin();\
    double m, w, xc, pc, dx, dp, r;\
    m = *iter;\
    w = *(++iter);\
    xc = *(++iter);\
    pc = *(++iter);\
    dx = *(++iter);\
    dp = *(++iter);\
    r = *(++iter);

double probabilityDistribution(double x, double p, double t, double m, double w,
                                 double xc, double pc, double dx, double dp, double r) {
    return exp(-((sqr(-xc + x*cos(w*t) - (p*sin(w*t))/(m*w))/sqr(dx) -
              (2*r*(-xc + x*cos(w*t) - (p*sin(w*t))/(m*w))*(-pc + p*cos(w*t) +
                     m*w*x*sin(w*t)))/(dp*dx) + sqr(-pc + p*cos(w*t) + m*w*x*sin(w*t))/
                  sqr(dp))/(2*(1 - sqr(r)))))/(2*dp*dx*Pi*sqrt(1 - sqr(r)));
}

double probabilityDistribution(double x, double p, double t,
                           const list<double> &initial) {
    init

    return probabilityDistribution(x, p, t, m, w, xc, pc, dx, dp, r);
}

double probabilityDistributionMax(const std::list<double> &initial) {
    init

    return probabilityDistribution(xc, pc, 0, m, w, xc, pc, dx, dp, r);
}

double probabilityDistributionX(double x, double t, double m, double w, double xc,
                            double pc, double dx, double dp, double r) {
    return exp(-(sqr(-m*w*x + m*w*xc*cos(w*t) + pc*sin(w*t))/
                (sqr(dp) + sqr(dx)*sqr(m)*sqr(w) + (-sqr(dp) + sqr(dx)*sqr(m)*sqr(w))*cos(2*w*t) +
                 2*dp*dx*m*w*r*sin(2*w*t))))/(dp*dx*sqrt(2*Pi)*sqrt(
                1 - sqr(r))*sqrt(-((
                 sqr(dx)*sqr(m)*sqr(w)*sqr(cos(w*t)) +
                  dp*(dp*sqr(sin(w*t)) + dx*m*w*r*sin(2*w*t)))/(
                 sqr(dp)*sqr(dx)*sqr(m)*sqr(w)*(-1 + sqr(r))))));
}

double probabilityDistributionX(double x, double t,
                            const std::list<double> &initial) {
    init

    return probabilityDistributionX(x, t, m, w, xc, pc, dx, dp, r);
}

double probabilityDistributionP(double p, double t, double m, double w, double xc,
                            double pc, double dx, double dp, double r) {
    return exp(-(sqr(p - pc*cos(w*t) + m*w*xc*sin(w*t))/(
                     sqr(dp) + sqr(dx)*sqr(m)*sqr(w) + (sqr(dp) - sqr(dx)*sqr(m)*sqr(w))*cos(2*w*t) -
                     2*dp*dx*m*w*r*sin(2*w*t))))/(dp*dx*sqrt(2*Pi)*sqrt(
                     1 - sqr(r))*sqrt((-sqr(dp)*sqr(cos(w*t)) +
                     dx*m*w*(-dx*m*w*sqr(sin(w*t)) + dp*r*sin(2*w*t)))/(
                     sqr(dp)*sqr(dx)*(-1 + sqr(r)))));
}

double probabilityDistributionP(double p, double t,
                            const std::list<double> &initial) {
    init

    return probabilityDistributionP(p, t, m, w, xc, pc, dx, dp, r);
}

double averageX(double t, double m, double w,
                        double xc, double pc, double dx, double dp, double r) {
    return xc*cos(w*t) +(pc/(m*w))*sin(w*t);
}

double averageX(double t, const list<double> &initial) {
    init

    return averageX(t, m, w, xc, pc, dx, dp, r);
}

double averageP(double t, double m, double w, double xc,
                                   double pc, double dx, double dp, double r) {
    return pc*cos(w*t) - m*w*xc*sin(w*t);
}

double averageP(double t, const std::list<double> &initial) {
    init

    return averageP(t, m, w, xc, pc, dx, dp, r);
}

} // namespace osc

namespace well {

#define init\
    auto iter = initial.cbegin();\
    double m, l, xc, pc, dx, dp;\
    m = *iter;\
    l = *(++iter);\
    xc = *(++iter);\
    pc = *(++iter);\
    dx = *(++iter);\
    dp = *(++iter);

double probabilityDistribution(double x, double p, double t, double m, double l,
                           double x0, double p0, double dx, double dp) {
    return exp(-sqr((2*(int(ceil((-t*p+x*m)/(l*m))) % 2)-1)*(-t*p/m-(ceil((-t*p+x*m)/(2*l*m))-(int(ceil((-t*p+x*m)/(l*m))) % 2))*2*l+x)-x0)/(2*sqr(dx))-(sqr((2*(int(ceil((-t*p+x*m)/(l*m))) % 2)-1)*p-p0))/(2*sqr(dp)))/(2*Pi*dx*dp);
}

double probabilityDistribution(double x, double p, double t,
                           const std::list<double> &initial) {
    init

    return probabilityDistribution(x, p, t, m, l, xc, pc, dx, dp);
}

double probabilityDistributionMax(const std::list<double> &initial) {
    init

    return probabilityDistribution(xc, pc, 0, m, l, xc, pc, dx, dp);
}

} // namespace well
} // namespace clasic

namespace quant {
namespace prt {

#define init\
    auto iter = initial.cbegin();\
    double m, xc, dx, h, kc;\
    m = *iter;\
    xc = *(++iter);\
    dx = *(++iter);\
    h = *(++iter);\
    kc = *(++iter);

double probabilityDistribution(double x, double p, double t, double m,
                               double xc, double dx, double h, double kc) {
    return exp(-((sqr(dx)*sqr(-h*kc + p))/(2*sqr(h))) - (2*sqr(p*t + m*(-x + xc)))/(
     sqr(dx)*sqr(m)))/(h*Pi);
}

double probabilityDistribution(double x, double p, double t,
                               const std::list<double> &initial) {
    init

    return probabilityDistribution(x, p, t, m, xc, dx, h, kc);
}

double probabilityDistributionX(double x, double t, double m,
                                double xc, double dx, double h, double kc) {
    return (sqrt(2)*dx*m*(exp(-((2*sqr(dx)*sqr(h*kc*t + m*(-x + xc)))/(
                                   pow(dx,4)*sqr(m) + 4*sqr(h)*sqr(t))))))/ sqrt(sqr(dx)*sqr(m)*Pi + 4*sqr(h)*Pi*sqr(t));
}

double probabilityDistributionX(double x, double t,
                                const std::list<double> &initial) {
    init

    return probabilityDistributionX(x, t, m, xc, dx, h, kc);
}

double probabilityDistributionP(double p, double t, double m,
                                double xc, double dx, double h, double kc) {
    return (dx*exp(-((sqr(dx)*sqr(-h*kc + p))/(2*sqr(h)))))/(h*sqrt(2*Pi));
}

double probabilityDistributionP(double p, double t,
                                const std::list<double> &initial) {
    init

    return probabilityDistributionP(p, t, m, xc, dx, h, kc);
}

double averageX(double t, double m, double xc, double dx, double h, double kc) {
    return (h*kc*t + m*xc)/m;
}

double averageX(double t, const std::list<double> &initial) {
    init

    return averageX(t, m, xc, dx, h, kc);
}

double averageP(double t, double m, double xc, double dx, double h, double kc) {
    return kc*h;
}

double averageP(double t, const std::list<double> &initial) {
    init

    return averageP(t, m, xc, dx, h, kc);
}

} // namespace prt

namespace statical {
#define init\
    auto iter = initial.cbegin();\
    double q0 = *iter;\
    double p0 = *(++iter);

namespace osc0 {

double probabilityDistribution(double x, double p, double t, double q0, double p0) {
    return exp(-sqr(p) - sqr(x))/(2*p0*Pi*q0);
}

double probabilityDistribution(double x, double p, double t,
                           const std::list<double> &initial) {
    init

    return probabilityDistribution(x, p, t, q0, p0);

}

double probabilityDistributionX(double x, double t, double q0, double p0) {
    return exp(-sqr(x))/(2*sqrt(Pi)*q0);
}

double probabilityDistributionX(double x, double t,
                            const std::list<double> &initial) {
    init

    return probabilityDistributionX(x, t, q0, p0);
}

double probabilityDistributionP(double p, double t, double q0, double p0) {
    return exp(-sqr(p))/(2*p0*sqrt(Pi));
}

double probabilityDistributionP(double p, double t,
                            const std::list<double> &initial) {
    init

    return probabilityDistributionP(p, t, q0, p0);
}

} // namespace osc0

namespace osc1 {

double probabilityDistribution(double x, double p, double t, double q0, double p0) {
    return (exp(-sqr(p) - sqr(x))*(-1 + 2*sqr(p)+ 2*sqr(x)))/(2*p0*Pi*q0);
}

double probabilityDistribution(double x, double p, double t,
                           const std::list<double> &initial) {
    init

    return probabilityDistribution(x, p, t, q0, p0);
}

double probabilityDistributionX(double x, double t, double q0, double p0) {
    return (exp(-sqr(x))*sqr(x))/(sqrt(Pi)*q0);
}

double probabilityDistributionX(double x, double t,
                            const std::list<double> &initial) {
    init

    return probabilityDistributionX(x, t, q0, p0);
}

double probabilityDistributionP(double p, double t, double q0, double p0) {
    return (exp(-sqr(p))*sqr(p))/(p0*sqrt(Pi));
}

double probabilityDistributionP(double p, double t,
                            const std::list<double> &initial) {
    init

    return probabilityDistributionP(p, t, q0, p0);
}

} // namespace osc1

namespace osc2 {

double probabilityDistribution(double x, double p, double t, double q0, double p0) {
    return (exp(-sqr(p) -
               sqr(x))*(1 - 4*sqr(x) + 2*(pow(p, 4) + pow(x, 4) + 2*sqr(p)*(-1 + sqr(x)))))/(2*p0*Pi*q0);
}

double probabilityDistribution(double x, double p, double t,
                           const std::list<double> &initial) {
    init

    return probabilityDistribution(x, p, t, q0, p0);
}

double probabilityDistributionX(double x, double t, double q0, double p0) {
    return (exp(-sqr(x))*sqr(1 - 2*sqr(x)))/(4*sqrt(Pi)*q0);
}

double probabilityDistributionX(double x, double t,
                            const std::list<double> &initial) {
    init
    return probabilityDistributionX(x, t, q0, p0);
}

double probabilityDistributionP(double p, double t, double q0, double p0) {
    return (exp(-sqr(p))*sqr(1 - 2*sqr(p)))/(4*p0*sqrt(Pi));
}

double probabilityDistributionP(double p, double t,
                            const std::list<double> &initial) {
    init

    return probabilityDistributionP(p, t, q0, p0);
}

} // namespace osc2

namespace osc3 {

double probabilityDistribution(double x, double p, double t, double q0, double p0) {
    return (exp(-sqr(p) -
               sqr(x))*(-3 + 4*pow(p, 6) + 18*sqr(x) - 18*pow(x, 4) + 4*pow(x, 6) + 6*pow(p, 4)*(-3 + 2*sqr(x)) +
                6*sqr(p)*(3 - 6*sqr(x) + 2*pow(x, 4))))/(6*p0*Pi*q0);
}

double probabilityDistribution(double x, double p, double t,
                           const std::list<double> &initial) {
    init

    return probabilityDistribution(x, p, t, q0, p0);
}

double probabilityDistributionX(double x, double t, double q0, double p0) {
    return (exp(-sqr(x))*sqr(x)*sqr(3 - 2*sqr(x)))/(6*sqrt(Pi)*q0);
}

double probabilityDistributionX(double x, double t,
                            const std::list<double> &initial) {
    init

    return probabilityDistributionX(x, t, q0, p0);
}

double probabilityDistributionP(double p, double t, double q0, double p0) {
    return (exp(-sqr(p))*sqr(p)*sqr(3 - 2*sqr(p)))/(6*p0*sqrt(Pi));
}

double probabilityDistributionP(double p, double t,
                            const std::list<double> &initial) {
    init

    return probabilityDistributionP(p, t, q0, p0);
}

} // namespace osc3

namespace osc4 {

double probabilityDistribution(double x, double p, double t, double q0, double p0) {
    return (exp(-sqr(p) -
              sqr(x))*(3 + 2*pow(p, 8) + 8*pow(p, 6)*(-2 + sqr(x)) +
               2*sqr(x)*(-2 + sqr(x))*(6 - 6*sqr(x) + pow(x, 4)) + 12*pow(p, 4)*(3 - 4*sqr(x) + pow(x, 4)) +
               8*sqr(p)*(-3 + sqr(x)*sqr(-3 + sqr(x)))))/(6*p0*Pi*q0);
}

double probabilityDistribution(double x, double p, double t, const std::list<double> &initial) {
    init

    return probabilityDistribution(x, p, t, q0, p0);
}

double probabilityDistributionX(double x, double t, double q0, double p0) {
    return (exp(-sqr(x))*sqr(3 + 4*sqr(x)*(-3 + sqr(x))))/(48*sqrt(Pi)*q0);
}

double probabilityDistributionX(double x, double t, const std::list<double> &initial) {
    init

    return probabilityDistributionX(x, t, q0, p0);
}

double probabilityDistributionP(double p, double t, double q0, double p0) {
    return (exp(-sqr(p))*sqr(3 + 4*sqr(p)*(-3 + sqr(p))))/(48*p0*sqrt(Pi));
}

double probabilityDistributionP(double p, double t, const std::list<double> &initial) {
    init

    return probabilityDistributionP(p, t, q0, p0);
}

} // namespace osc4
} // namespace statical

namespace oscdynam {
#define init\
    auto iter = initial.cbegin();\
    double q0 = *iter;\
    double p0 = *(++iter);\
    double o = *(++iter);\
    double C0 = *(++iter);\
    double C1 = *(++iter);\
    double C2 = *(++iter);\
    double C3 = *(++iter);

double probabilityDistribution(double x, double p, double t, double q0,
                               double p0, double o, double C0,
                               double C1, double C2, double C3) {
    return (1/(6*(sqr(C0) + sqr(C1) + sqr(C2) + sqr(C3))*p0*Pi*q0))*exp(-sqr(p) -
       sqr(x))*(3*sqr(C0) + 3*(C2 - C3)*(C2 + C3) + sqr(C1)*(-3 + 6*sqr(p) + 6*sqr(x)) +
        2*(sqr(p) + sqr(x))*(3*sqr(C2)*(-2 + sqr(p) + sqr(x)) +
           sqr(C3)*(-3 + sqr(p) + sqr(x))*(-3 + 2*sqr(p) + 2*sqr(x))) +
        2*x*(3*sqrt(2)*C0*C1 +
           C2*(3*sqrt(6)*C3 + 6*C1*(-1 + sqr(p) + sqr(x)) +
              2*sqrt(6)*C3*(-3 + sqr(p) + sqr(x))*(sqr(p) + sqr(x))))*cos(o*t) -
        2*sqrt(2)*(p - x)*(p + x)*(3*C0*C2 +
           sqrt(3)*C1*C3*(-3 + 2*sqr(p) + 2*sqr(x)))*cos(2*o*t) +
        4*sqrt(3)*C0*C3*x*(-3*sqr(p) + sqr(x))*cos(3*o*t) -
        2*p*(3*sqrt(2)*C0*C1 +
           C2*(3*sqrt(6)*C3 + 6*C1*(-1 + sqr(p) + sqr(x)) +
              2*sqrt(6)*C3*(-3 + sqr(p) + sqr(x))*(sqr(p) + sqr(x))))*sin(o*t) -
        4*sqrt(2)*p*x*(3*C0*C2 + sqrt(3)*C1*C3*(-3 + 2*sqr(p) + 2*sqr(x)))*sin(2*o*t) +
        4*sqrt(3)*C0*C3*p*(sqr(p) - 3*sqr(x))*sin(3*o*t));
}

double probabilityDistribution(double x, double p, double t,
                               const std::list<double> &initial) {
    init

    return probabilityDistribution(x, p, t, q0, p0, o, C0, C1, C2, C3);
}

} // namespace oscdynam

namespace well {
#define init\
    auto iter = initial.cbegin();\
    double a = *iter;\
    double n = *(++iter);

double probabilityDistribution(double x, double p,
                                     double t, double a, double n) {
    double h = 1;
    if ((x > a/2.) && (a > x)) {
        return ((sqr(a)*sqr(p)*cos((2*n*Pi*(a - x))/
                a) + (-sqr(a)*sqr(p) + sqr(h)*sqr(n)*sqr(Pi))*cos((2*n*Pi*x)/a))*sin((
             2*p*(a - x))/h) -
           a*h*n*p*Pi*cos((2*p*(a - x))/h)*sin((2*n *Pi*(a - x))/
             a))/(a*p*Pi*(a*p - h*n*Pi)*(a*p + h*n*Pi));
    }
    else if ((x > 0) && (a >= 2*x)) {
        return (h*n*(h*n*Pi*cos((2*n*Pi*x)/a)*sin((2*p*x)/h) -
                a*p*cos((2*p*x)/h)*sin((2*n*Pi*x)/a)))/(
             pow(a, 3)*pow(p, 3) - a*sqr(h)*sqr(n)*p*sqr(Pi));
    }

    return 0;
}

double probabilityDistribution(double x, double p, double t,
                                     const std::list<double> &initial) {
    init

    return probabilityDistribution(x, p, t, a, n);
}

double probabilityDistributionX(double x, double t,
                                double a, double n) {
    if ((0 <= x) && (x <= a)) {
        return (2/a)*sqr(sin(Pi*n*x/a));
    }

    return 0;
}

double probabilityDistributionX(double x, double t,
                                const std::list<double> &initial) {
    init

    return probabilityDistributionX(x, t, a, n);
}

double probabilityDistributionP(double p, double t,
                                double a, double n) {
    double h = 1;
    return (2*a*pow(h, 3)*sqr(n)*Pi*(1-cos(a*p/h)*cos(Pi*n)))/
            (sqr(a*p-Pi*n*h)*sqr(a*p+Pi*n*h));
}

double probabilityDistributionP(double p, double t,
                                const std::list<double> &initial) {
    init

    return probabilityDistributionP(p, t, a, n);
}

double averageX(double t, double a, double n) {
    return a/2;
}

double averageX(double t, const std::list<double> &initial) {
    init

    return averageX(t, a, n);
}

} // namespace well
} // namespace quant
