// Copyright 2017 <Ilia Minchenko>
#pragma once

#include <list>

double zeroFunction();
double zeroFunction(double a, const std::list<double> &lst);
double zeroFunction(double a, double b, const std::list<double> &lst);
double zeroFunction(double a, double b, double c, const std::list<double> &lst);

namespace classic {
// simple particle
namespace prt {
double probabilityDistribution(double x, double p, double t, double m,
                                 double xc, double pc, double dx, double dp, double r);

double probabilityDistribution(double x, double p, double t,
                           const std::list<double> &initial);
double probabilityDistributionMax(const std::list<double> &initial);


double probabilityDistributionX(double x, double t, double m,
                            double xc, double pc, double dx, double dp, double r);
double probabilityDistributionX(double x, double t,
                            const std::list<double> &initial);

double probabilityDistributionP(double p, double t, double m,
                            double xc, double pc, double dx, double dp, double r);
double probabilityDistributionP(double p, double t,
                            const std::list<double> &initial);


double averageX(double t, double m, double xc, double pc,
                double dx, double dp, double r);
double averageX(double t, const std::list<double> &initial);

double averageP(double t, double m, double xc, double pc,
                double dx, double dp, double r);
double averageP(double t, const std::list<double> &initial);
} // namespace prt


// oscillator
namespace osc {
double probabilityDistribution(double x, double p, double t, double m, double w,
                                 double xc, double pc, double dx, double dp, double r);
double probabilityDistribution(double x, double p, double t,
                           const std::list<double> &initial);
double probabilityDistributionMax(const std::list<double> &initial);


double probabilityDistributionX(double x, double t, double m, double w,
                            double xc, double pc, double dx, double dp, double r);
double probabilityDistributionX(double x, double t,
                            const std::list<double> &initial);

double probabilityDistributionP(double p, double t, double m, double w,
                            double xc, double pc, double dx, double dp, double r);
double probabilityDistributionP(double p, double t,
                            const std::list<double> &initial);


double averageX(double t, double m, double w,
                            double xc, double pc, double dx, double dp, double r);
double averageX(double t,
                            const std::list<double> &initial);

double averageP(double t, double m, double w,
                            double xc, double pc, double dx, double dp, double r);
double averageP(double t,
                            const std::list<double> &initial);

} // namespace osc

// potential well
namespace well {
double probabilityDistribution(double x, double p, double t, double m, double l,
                           double xc, double pc, double dx, double dp);
double probabilityDistribution(double x, double p, double t,
                           const std::list<double> &initial);
double probabilityDistributionMax(const std::list<double> &initial);
} // namespace well

} // namespace classic

namespace quant {

// particle
namespace prt {
double probabilityDistribution(double x, double p, double t, double m,
                                 double xc, double dx, double h, double kc);

double probabilityDistribution(double x, double p, double t,
                           const std::list<double> &initial);


double probabilityDistributionX(double x, double t, double m,
                            double xc, double dx, double h, double kc);
double probabilityDistributionX(double x, double t,
                            const std::list<double> &initial);

double probabilityDistributionP(double p, double t, double m,
                            double xc, double dx,  double h, double kc);
double probabilityDistributionP(double p, double t,
                            const std::list<double> &initial);


double averageX(double t, double m, double xc,
                double dx, double h, double kc);
double averageX(double t, const std::list<double> &initial);

double averageP(double t, double m, double xc,
                double dx,  double h, double kc);
double averageP(double t, const std::list<double> &initial);
} // namespace prt

// oscillators
namespace statical {
namespace osc0 {
double probabilityDistribution(double x, double p, double t, double q0, double p0);
double probabilityDistribution(double x, double p, double t,
                           const std::list<double> &initial);


double probabilityDistributionX(double x, double t, double q0, double p0);
double probabilityDistributionX(double x, double t,
                            const std::list<double> &initial);

double probabilityDistributionP(double p, double t, double q0, double p0);
double probabilityDistributionP(double p, double t,
                            const std::list<double> &initial);
} // namescape osc0

namespace osc1 {
double probabilityDistribution(double x, double p, double t, double q0, double p0);
double probabilityDistribution(double x, double p, double t,
                           const std::list<double> &initial);


double probabilityDistributionX(double x, double t, double q0, double p0);
double probabilityDistributionX(double x, double t,
                            const std::list<double> &initial);

double probabilityDistributionP(double p, double t, double q0, double p0);
double probabilityDistributionP(double p, double t,
                            const std::list<double> &initial);
} // namescape osc1

namespace osc2 {
double probabilityDistribution(double x, double p, double t, double q0, double p0);
double probabilityDistribution(double x, double p, double t,
                           const std::list<double> &initial);


double probabilityDistributionX(double x, double t, double q0, double p0);
double probabilityDistributionX(double x, double t,
                            const std::list<double> &initial);

double probabilityDistributionP(double p, double t, double q0, double p0);
double probabilityDistributionP(double p, double t,
                            const std::list<double> &initial);
} // namescape osc2

namespace osc3 {
double probabilityDistribution(double x, double p, double t, double q0, double p0);
double probabilityDistribution(double x, double p, double t,
                           const std::list<double> &initial);


double probabilityDistributionX(double x, double t, double q0, double p0);
double probabilityDistributionX(double x, double t,
                            const std::list<double> &initial);

double probabilityDistributionP(double p, double t, double q0, double p0);
double probabilityDistributionP(double p, double t,
                            const std::list<double> &initial);
} // namescape osc3

namespace osc4 {

double probabilityDistribution(double x, double p, double t, double q0, double p0);
double probabilityDistribution(double x, double p, double t,
                           const std::list<double> &initial);


double probabilityDistributionX(double x, double t, double q0, double p0);
double probabilityDistributionX(double x, double t,
                            const std::list<double> &initial);

double probabilityDistributionP(double p, double t, double q0, double p0);
double probabilityDistributionP(double p, double t,
                            const std::list<double> &initial);
} // namespace osc4
} // namespace statical

namespace oscdynam {

double probabilityDistribution(double x, double p, double t, double q0,
                               double p0, double o, double C0,
                               double C1, double C2, double C3);
double probabilityDistribution(double x, double p, double t,
                               const std::list<double> &initial);

} // namespace oscdynam

namespace well {
double probabilityDistribution(double x, double p, double t, double a, double n);
double probabilityDistribution(double x, double p, double t,
                           const std::list<double> &initial);

double probabilityDistributionX(double x, double t, double a, double n);
double probabilityDistributionX(double x, double t,
                           const std::list<double> &initial);

double probabilityDistributionP(double p, double t, double a, double n);
double probabilityDistributionP(double p, double t,
                           const std::list<double> &initial);

double averageX(double t, double a, double n);
double averageX(double t, const std::list<double> &initial);
} // namespace well

} // namespace quant

