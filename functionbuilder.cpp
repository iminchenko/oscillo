// Copyright 2017 <Ilia Minchenko>
#include "functionbuilder.h"
#include "function.h"

using std::vector;
using std::list;

double calcMax(double (*func)(double x,
                                   const list<double> &conditions),
               const list<double> &conditions,
               double rmin, double rmax, double step) {
    double max = func(0, conditions);
    for (double x = rmin; x < rmax; x += step) {
        double f = func(x,conditions);
        max = std::max(max, f);
    }

    return max;
}

double calcMax(double (*func)(double x, double t,
                                   const list<double> &conditions),
               const list<double> &conditions,
               double rmin, double rmax, double step) {
    double max = func(0, 0, conditions);
    for (double x = rmin; x < rmax; x += step)
        for (double t = 0; t < 5; t += 0.1) {
            double f = func(x, t, conditions);
            max = std::max(max, f);
        }

    return max;
}

double calcMax(double (*func)(double x, double p, double t,
                                   const list<double> &conditions),
               const list<double> &conditions,
               double rminx, double rmaxx, double rminp, double rmaxp,
               double step) {
    double max = func(0, 0, 0, conditions);
    for (double x = rminx; x < rmaxx; x += step)
        for (double p = rminp; p < rmaxp; p += step)
            for (double t = 0; t < 6; t += 0.7) {
                double f = func(x, p, t, conditions);
                max = std::max(max, f);
            }

    return max;
}

double calcMin(double (*func)(double x,
                                   const list<double> &conditions),
               const list<double> &conditions,
               double rmin, double rmax, double step) {
    double min = func(0, conditions);
    for (double x = rmin; x < rmax; x += step) {
        double f = func(x,conditions);
        min = std::min(min, f);
    }

    return min;
}

double calcMin(double (*func)(double x, double t,
                                   const list<double> &conditions),
               const list<double> &conditions,
               double rmin, double rmax, double step) {
    double min = func(0, 0, conditions);
    for (double x = rmin; x < rmax; x += step)
        for (double t = 0; t < 5; t += 0.1) {
            double f = func(x, t, conditions);
            min = std::min(min, f);
        }

    return min;
}

double calcMin(double (*func)(double x, double p, double t,
                                   const list<double> &conditions),
               const list<double> &conditions,
               double rminx, double rmaxx, double rminp, double rmaxp,
               double step) {
    double min = func(0, 0, 0, conditions);
    for (double x = rminx; x < rmaxx; x += step)
        for (double p = rminp; p < rmaxp; p += step)
            for (double t = 0; t < 6; t += 0.7) {
                double f = func(x, p, t, conditions);
                min = std::min(min, f);
            }

    return min;
}

FunctionBuilder::FunctionBuilder()
    :_limitsCalculated(false) {
    setMode(Mode(0));
}

FunctionBuilder::matrix FunctionBuilder::build3DFunction() const {
    if (_probabilityDistribution == nullptr)
        return matrix();

    int sizeX = (_rangeMaxX - _rangeMinX)/_step3D;
    int sizeP = (_rangeMaxP - _rangeMinP)/_step3D;
    ++sizeX;
    ++sizeP;
    matrix m(sizeX, vector<QVector3D>(sizeP));

    double x = _rangeMinX;
    for (size_t i = 0; i < sizeX; ++i) {
        double p = _rangeMinP;
        for (size_t j = 0; j < sizeP; ++j) {
            m[i][j] = QVector3D(x,
                                _probabilityDistribution(x, p, _t, _initialConditions),
                                p);
            p += _step3D;
        }
        x += _step3D;
    }

    return std::move(m);
}

std::vector<QPointF> FunctionBuilder::buildWxFunction() const {
    if (!_probabilityDistributionX)
        return vector<QPointF>();

    int size = (_rangeMaxX - _rangeMinX)/_step2D;
    ++size;
    vector<QPointF> v(size);

    double x = _rangeMinX;
    for (size_t i = 0; i < v.size(); ++i) {
        v[i] = QPointF(x, _probabilityDistributionX(x, _t, _initialConditions));
        x += _step2D;
    }

    return std::move(v);
}

std::vector<QPointF> FunctionBuilder::buildWpFunction() const {
    if (!_probabilityDistributionP)
        return vector<QPointF>();

    double size = (_rangeMaxP - _rangeMinP)/_step2D;
    ++size;
    vector<QPointF> v(size);

    double p = _rangeMinP;
    for (size_t i = 0; i < v.size(); ++i) {
        v[i] = QPointF(p, _probabilityDistributionP(p, _t, _initialConditions));
        p += _step2D;
    }

    return std::move(v);
}

std::vector<QPointF> FunctionBuilder::buildXAverageFunction() const {
    if (!_averageX)
        return vector<QPointF>();

    double size = (_rangeMaxAverage - _rangeMinAverage)/_step2D;
    ++size;
    vector<QPointF> v(size);

    double t = _rangeMinAverage;
    for (size_t i = 0; i < v.size(); ++i) {
        v[i] = QPointF(t, _averageX(t + _t, _initialConditions));
        t += _step2D;
    }

    return std::move(v);
}

std::vector<QPointF> FunctionBuilder::buildPAverageFunction() const {
    if (!_averageP)
        return vector<QPointF>();

    double size = (_rangeMaxAverage - _rangeMinAverage)/_step2D;
    ++size;
    vector<QPointF> v(size);

    double t = _rangeMinAverage;
    for (size_t i = 0; i < v.size(); ++i) {
        v[i] = QPointF(t, _averageP(t + _t, _initialConditions));
        t += _step2D;
    }

    return std::move(v);
}

double FunctionBuilder::getMaxValueW3D() const {
    calculateLimits();

    return _maxValueW3D;
}

double FunctionBuilder::getMinValueW3D() const {
    calculateLimits();

    return _minValueW3D;
}

double FunctionBuilder::getMaxValueWx() const {
    calculateLimits();

    return _maxValueWx;
}

double FunctionBuilder::getMinValueWx() const {
    calculateLimits();

    return _minValueWx;
}

double FunctionBuilder::getMaxValueWp() const {
    calculateLimits();

    return _maxValueWp;
}

double FunctionBuilder::getMinValueWp() const {
    calculateLimits();

    return _minValueWp;
}

double FunctionBuilder::getMaxValueAverageX() const {
    calculateLimits();

    return _maxValueAverageX;
}

double FunctionBuilder::getMinValueAverageX() const {
    calculateLimits();

    return _minValueAverageX;
}

double FunctionBuilder::getMaxValueAverageP() const {
    calculateLimits();

    return _maxValueAverageP;
}

double FunctionBuilder::getMinValueAverageP() const {
    calculateLimits();

    return _minValueAverageP;
}

void FunctionBuilder::setStep3D(double step) {
    if (_step3D > step)
        _limitsCalculated = false;

    _step3D = step;
}

void FunctionBuilder::setStep2D(double step) {
    if (_step2D > step)
        _limitsCalculated = false;

    _step2D = step;
}

void FunctionBuilder::setRangeX(double min, double max) {
    if (_rangeMinX > min || _rangeMaxX < max)
        _limitsCalculated = false;

    _rangeMinX = min;
    _rangeMaxX = max;
}

void FunctionBuilder::setRangeP(double min, double max) {
    if (_rangeMinP > min || _rangeMaxP < max)
        _limitsCalculated = false;

    _rangeMinP = min;
    _rangeMaxP = max;
}

void FunctionBuilder::setRangeAverage(double min, double max) {
    if (_rangeMinAverage > min || _rangeMaxAverage < max)
        _limitsCalculated = false;

    _rangeMinAverage = min;
    _rangeMaxAverage = max;
}

void FunctionBuilder::setT(double t) {
    _t = t;
}

void FunctionBuilder::addT(double t) {
    _t += t;
}

void FunctionBuilder::setInitialConditions(std::list<double> conditions) {
    if (_initialConditions != conditions)
        _limitsCalculated = false;

    _initialConditions = std::move(conditions);
}

void FunctionBuilder::setMode(Mode mode) {
    _limitsCalculated = false;

    _probabilityDistribution = nullptr;
    _probabilityDistributionMax = nullptr;
    _probabilityDistributionMin = nullptr;

    _probabilityDistributionX = nullptr;
    _probabilityDistributionXMax = nullptr;
    _probabilityDistributionXMin = nullptr;

    _probabilityDistributionP = nullptr;
    _probabilityDistributionPMax = nullptr;
    _probabilityDistributionPMin = nullptr;

    _averageX = nullptr;
    _averageXMax = nullptr;
    _averageXMin = nullptr;

    _averageP = nullptr;
    _averagePMax = nullptr;
    _averagePMin = nullptr;

    switch(mode) {
    case Mode::cParticle:
        _probabilityDistribution = &(classic::prt::probabilityDistribution);
        _probabilityDistributionMax = &(classic::prt::probabilityDistributionMax);

        _probabilityDistributionX = &(classic::prt::probabilityDistributionX);

        _probabilityDistributionP = &(classic::prt::probabilityDistributionP);

        _averageX = &(classic::prt::averageX);

        _averageP = &(classic::prt::averageP);
        break;

    case Mode::qParticle:
        _probabilityDistribution = &(quant::prt::probabilityDistribution);

        _probabilityDistributionX = &(quant::prt::probabilityDistributionX);

        _probabilityDistributionP = &(quant::prt::probabilityDistributionP);

        _averageX = &(quant::prt::averageX);

        _averageP = &(quant::prt::averageP);
        break;

    case Mode::cOscillator:
        _probabilityDistribution = &(classic::osc::probabilityDistribution);
        _probabilityDistributionMax = &(classic::osc::probabilityDistributionMax);

        _probabilityDistributionX = &(classic::osc::probabilityDistributionX);

        _probabilityDistributionP = &(classic::osc::probabilityDistributionP);

        _averageX = &(classic::osc::averageX);

        _averageP = &(classic::osc::averageP);
        break;

    case Mode::qOscillator0:
        _probabilityDistribution = &(quant::statical::osc0::probabilityDistribution);

        _probabilityDistributionX = &(quant::statical::osc0::probabilityDistributionX);

        _probabilityDistributionP = &(quant::statical::osc0::probabilityDistributionP);

        _averageX = &(zeroFunction);

        _averageP = &(zeroFunction);
        break;

    case Mode::qOscillator1:
        _probabilityDistribution = &(quant::statical::osc1::probabilityDistribution);

        _probabilityDistributionX = &(quant::statical::osc1::probabilityDistributionX);

        _probabilityDistributionP = &(quant::statical::osc1::probabilityDistributionP);

        _averageX = &(zeroFunction);

        _averageP = &(zeroFunction);
        break;

    case Mode::qOscillator2:
        _probabilityDistribution = &(quant::statical::osc2::probabilityDistribution);

        _probabilityDistributionX = &(quant::statical::osc2::probabilityDistributionX);

        _probabilityDistributionP = &(quant::statical::osc2::probabilityDistributionP);

        _averageX = &(zeroFunction);

        _averageP = &(zeroFunction);
        break;

    case Mode::qOscillator3:
        _probabilityDistribution = &(quant::statical::osc3::probabilityDistribution);

        _probabilityDistributionX = &(quant::statical::osc3::probabilityDistributionX);

        _probabilityDistributionP = &(quant::statical::osc3::probabilityDistributionP);

        _averageX = &(zeroFunction);

        _averageP = &(zeroFunction);
        break;

    case Mode::qOscillator4:
        _probabilityDistribution = &(quant::statical::osc4::probabilityDistribution);

        _probabilityDistributionX = &(quant::statical::osc4::probabilityDistributionX);

        _probabilityDistributionP = &(quant::statical::osc4::probabilityDistributionP);

        _averageX = &(zeroFunction);

        _averageP = &(zeroFunction);
        break;

    case Mode::qOscDynamic:
        _probabilityDistribution = &(quant::oscdynam::probabilityDistribution);
        break;

    case Mode::cWell:
        _probabilityDistribution = &(classic::well::probabilityDistribution);
        _probabilityDistributionMax = &(classic::well::probabilityDistributionMax);
        break;

    case Mode::qWell:
        _probabilityDistribution = &(quant::well::probabilityDistribution);

        _probabilityDistributionX = &(quant::well::probabilityDistributionX);

        _probabilityDistributionP = &(quant::well::probabilityDistributionP);

        _averageX = &(quant::well::averageX);

        _averageP = &(zeroFunction);
        break;
    }
}

//template<class A, class B, class C, class D>
//double templateCalcLimit(A functionMax, B function, C limitFunction) {
//    if (functionMax == nullptr) {
//        if (function == nullptr)
//            return 1;
//        return limitFunction(_probabilityDistribution, _initialConditions);
//    }
//    return _functionMax(_initialConditions);
//}

void FunctionBuilder::calculateLimits() const {
    if (_limitsCalculated)
        return;

    if (_probabilityDistributionMax == nullptr) {
        if (_probabilityDistribution == nullptr)
            _maxValueW3D =  1;
        else
            _maxValueW3D = calcMax(_probabilityDistribution, _initialConditions,
                                   _rangeMinX, _rangeMaxX, _rangeMinP,
                                   _rangeMaxP, _step3D);
    }
    else {
        _maxValueW3D =  _probabilityDistributionMax(_initialConditions);
    }
    // ------------------
    if (_probabilityDistributionMin == nullptr) {
        if (_probabilityDistribution == nullptr)
            _minValueW3D = 0;
        else
            _minValueW3D = calcMin(_probabilityDistribution, _initialConditions,
                                   _rangeMinX, _rangeMaxX, _rangeMinP,
                                   _rangeMaxP, _step3D);
    }
    else {
        _minValueW3D = _probabilityDistributionMin(_initialConditions);
    }
    // ------------------
    if (_probabilityDistributionXMax == nullptr) {
        if (_probabilityDistributionX == nullptr)
            _maxValueWx = 1;
        else
            _maxValueWx = calcMax(_probabilityDistributionX, _initialConditions,
                                  _rangeMinX, _rangeMaxX, _step2D);
    }
    else {
        _maxValueWx = _probabilityDistributionXMax(_initialConditions);
    }
    // ------------------
    if (_probabilityDistributionXMin == nullptr) {
        if (_probabilityDistributionX == nullptr)
            _minValueWx = 0;
        else
            _minValueWx = calcMin(_probabilityDistributionX, _initialConditions,
                                  _rangeMinX, _rangeMaxX, _step2D);
    }
    else {
        _minValueWx = _probabilityDistributionXMin(_initialConditions);
    }
    // ------------------
    if (_probabilityDistributionPMax == nullptr) {
        if (_probabilityDistributionP == nullptr)
            _maxValueWp = 1;
        else
            _maxValueWp = calcMax(_probabilityDistributionP, _initialConditions,
                                  _rangeMinP, _rangeMaxP, _step2D);
    }
    else {
        _maxValueWp = _probabilityDistributionPMax(_initialConditions);
    }
    // ------------------
    if (_probabilityDistributionPMin == nullptr) {
        if (_probabilityDistributionP == nullptr)
            _minValueWp = 0;
        else
            _minValueWp = calcMin(_probabilityDistributionP, _initialConditions,
                                  _rangeMinP, _rangeMaxP, _step2D);
    }
    else {
        _minValueWp = _probabilityDistributionPMin(_initialConditions);
    }
    // ------------------
    if (_averageXMax == nullptr) {
        if (_averageX == nullptr)
            _maxValueAverageX = 1;
        else
            _maxValueAverageX = calcMax(_averageX, _initialConditions,
                                        _rangeMinX, _rangeMaxX, _step2D);
    }
    else {
        _maxValueAverageX = _averageXMax(_initialConditions);
    }
    // ------------------
    if (_averageXMin == nullptr) {
        if (_averageX == nullptr)
            _minValueAverageX = 0;
        else
            _minValueAverageX = calcMin(_averageX, _initialConditions,
                                        _rangeMinX, _rangeMaxX, _step2D);
    }
    else {
        _minValueAverageX = _averageXMin(_initialConditions);
    }
    // ------------------
    if (_averagePMax == nullptr) {
        if (_averageP == nullptr)
            _maxValueAverageP = 1;
        else
            _maxValueAverageP = calcMax(_averageP, _initialConditions,
                                        _rangeMinP, _rangeMaxP, _step2D);
    }
    else {
        _maxValueAverageP = _averagePMax(_initialConditions);
    }
    // ------------------
    if (_averagePMin == nullptr) {
        if (_averageP == nullptr)
            _minValueAverageP = 0;
        else
            _minValueAverageP = calcMin(_averageP, _initialConditions,
                                        _rangeMinP, _rangeMaxP, _step2D);
    }
    else {
        _minValueAverageP = _averagePMin(_initialConditions);
    }

    _limitsCalculated = true;
}
