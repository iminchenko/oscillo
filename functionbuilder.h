// Copyright 2017 <Ilia Minchenko>
#pragma once

#include <QVector3D>
#include <QPointF>
#include <vector>
#include <list>
#include <functional>

#include "modes.h"

class FunctionBuilder {
    using matrix = std::vector<std::vector<QVector3D> >;
public:
    FunctionBuilder();

    matrix build3DFunction() const;
    std::vector<QPointF> buildWxFunction() const;
    std::vector<QPointF> buildWpFunction() const;
    std::vector<QPointF> buildXAverageFunction() const;
    std::vector<QPointF> buildPAverageFunction() const;

    double getMaxValueW3D() const;
    double getMinValueW3D() const;
    double getMaxValueWx() const;
    double getMinValueWx() const;
    double getMaxValueWp() const;
    double getMinValueWp() const;
    double getMaxValueAverageX() const;
    double getMinValueAverageX() const;
    double getMaxValueAverageP() const;
    double getMinValueAverageP() const;

    void setStep3D(double step);
    void setStep2D(double step);
    void setRangeX(double min, double max);
    void setRangeP(double min, double max);
    void setRangeAverage(double min, double max);
    void setT(double t);
    void addT(double t);

    void setInitialConditions(std::list<double> conditions);

    void setMode(Mode mode);

private:
    // calculating all maximums and minimums
    void calculateLimits() const;

    double _step3D;
    double _step2D;
    double _rangeMinX;
    double _rangeMaxX;
    double _rangeMinP;
    double _rangeMaxP;
    double _rangeMinAverage;
    double _rangeMaxAverage;
    double _t;

    // optimized maximum and minimum calculating
    mutable bool _limitsCalculated;
    mutable double _maxValueW3D;
    mutable double _minValueW3D;
    mutable double _maxValueWx;
    mutable double _minValueWx;
    mutable double _maxValueWp;
    mutable double _minValueWp;
    mutable double _maxValueAverageX;
    mutable double _minValueAverageX;
    mutable double _maxValueAverageP;
    mutable double _minValueAverageP;

    std::list<double> _initialConditions;

    double (*_probabilityDistribution)(double x, double p, double t,
                               const std::list<double> &conditions);
    double (*_probabilityDistributionMax)(const std::list<double> &conditions);
    double (*_probabilityDistributionMin)(const std::list<double> &conditions);

    double (*_probabilityDistributionX)(double x, double t,
                               const std::list<double> &conditions);
    double (*_probabilityDistributionXMax)(const std::list<double> &conditions);
    double (*_probabilityDistributionXMin)(const std::list<double> &conditions);

    double (*_probabilityDistributionP)(double p, double t,
                               const std::list<double> &conditions);
    double (*_probabilityDistributionPMax)(const std::list<double> &conditions);
    double (*_probabilityDistributionPMin)(const std::list<double> &conditions);

    double (*_averageX)(double t,
                               const std::list<double> &conditions);
    double (*_averageXMax)(const std::list<double> &conditions);
    double (*_averageXMin)(const std::list<double> &conditions);

    double (*_averageP)(double t,
                               const std::list<double> &conditions);
    double (*_averagePMax)(const std::list<double> &conditions);
    double (*_averagePMin)(const std::list<double> &conditions);
};
