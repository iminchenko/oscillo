// Copyright 2017 <Ilia Minchenko>
#include <QLabel>
#include <QDoubleSpinBox>
#include <QSpinBox>

#include "inputwidget.h"

using std::list;

QDoubleSpinBox *createDoubleSpinbox() {
    QDoubleSpinBox *spinbox = new QDoubleSpinBox();
    spinbox->setValue(0);
    spinbox->setRange(-999.999, 999.999);
    spinbox->setDecimals(3);
    spinbox->setSingleStep(0.01);
    //spinbox->setFrame(false);
    // for dot instead comma
    spinbox->setLocale(QLocale("C"));

    return spinbox;
}

InputWidget::InputWidget(QWidget *parent)
    :QGroupBox(parent) {
    _mainLayout = nullptr;
    clearInput();
}

InputWidget::InputWidget(const QString &title, QWidget *parent)
    :QGroupBox(title, parent) {
    _mainLayout = nullptr;
    clearInput();
}

void InputWidget::setFields(const QStringList &fields) {
    clearInput();

    int counter = 0;

    for (const auto &iter : fields) {
        _labels.push_back(new QLabel(iter + " ="));
        _fields.push_back(createDoubleSpinbox());
        connect(_fields.back(), SIGNAL(editingFinished()),
                this, SIGNAL(valueEdited()));

        _mainLayout->addWidget(_labels.back(), counter, 0);
        _mainLayout->addWidget(_fields.back(), counter, 1);

        ++counter;
    }

    this->setMinimumHeight(counter*31 + 40);
}

void InputWidget::setValues(const std::list<double> &values) {
    auto iterValue = values.cbegin();
    auto iterSpinbox = _fields.begin();

    while ((iterValue != values.cend()) && (iterSpinbox != _fields.cend())) {
        (*iterSpinbox)->setValue(*iterValue);
        ++iterSpinbox;
        ++iterValue;
    }
}

void InputWidget::setInteger(const std::list<bool> &integer) {
    auto iterInt = integer.cbegin();
    auto iterSpinbox = _fields.begin();

    while ((iterInt != integer.cend()) && (iterSpinbox != _fields.cend())) {
        if (*iterInt) {
            (*iterSpinbox)->setDecimals(0);
            (*iterSpinbox)->setSingleStep(1);
        }
        else {
            (*iterSpinbox)->setDecimals(3);
            (*iterSpinbox)->setSingleStep(0.01);
        }
        ++iterSpinbox;
        ++iterInt;
    }
}

std::list<double> InputWidget::getValues() const {
    list<double> values;

    for (const auto &iter : _fields) {
        values.push_back(iter->value());
    }

    return std::move(values);
}

void InputWidget::clearInput() {
    for (auto &iter : _fields)
        delete iter;
    for (auto &iter : _labels)
        delete iter;

    if (_mainLayout) {
        delete _mainLayout;
        _mainLayout = nullptr;
    }
    _mainLayout = new QGridLayout(this);

    _fields.clear();
    _labels.clear();
}
