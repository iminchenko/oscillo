// Copyright 2017 <Ilia Minchenko>
#pragma once

#include <QGroupBox>
#include <QDoubleSpinBox>
#include <QGridLayout>
#include <QStringList>
#include <list>

class InputWidget : public QGroupBox {
    Q_OBJECT
public:

    InputWidget(QWidget *parent = Q_NULLPTR);
    InputWidget(const QString &title, QWidget *parent = Q_NULLPTR);

    void setFields(const QStringList &fields);
    void setValues(const std::list<double> &values);
    // which spinboxes should be integer
    void setInteger(const std::list<bool> &integer);

    std::list<double> getValues() const;

private:
    void clearInput();

    std::list<QDoubleSpinBox *> _fields;
    std::list<QWidget *> _labels;
    QGridLayout *_mainLayout;

signals:
    void valueEdited();
};
