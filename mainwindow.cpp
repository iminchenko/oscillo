// Copyright 2017 <Ilia Minchenko>
#include <exception>
#include <QMessageBox>
#include <QtCharts/QLineSeries>

#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow) {
    ui->setupUi(this);

    _plotsWidget = new PlotsWidget(this);
    ui->centralWidget->layout()->addWidget(_plotsWidget);

    _timerIdStep = -1;
    _timerIdCamera = -1;
    _timerIdPresentation = -1;
    _timerIdPreparation = -1;
    _paused = false;
    _simulating = false;
    _prepared = false;

    prepareToSimulation();
    setMode(Mode(ui->comboBox_mode->currentIndex()));
    setStyle(Style(ui->comboBox_style->currentIndex()));

    ui->comboBox_mode->setCurrentIndex(1);
    ui->comboBox_mode->setCurrentIndex(0);
}

MainWindow::~MainWindow() {
    delete ui;
}

void MainWindow::timerEvent(QTimerEvent *event) {
    if (event->timerId() == _timerIdStep) {
        try {
            _funcBuilder.addT(0.05);

            drawPlots();
        }
        catch (const std::exception &e) {
            // stopping timer
            on_pushButton_stop_clicked();

            QMessageBox::warning(this, "Error", e.what());
        }
    }
    else if (event->timerId() == _timerIdCamera) {
        _plotsWidget->rotateCamera(1);
    } else if (event->timerId() == _timerIdPresentation) {
        presentationStep();
    } else if (event->timerId() == _timerIdPreparation) {
        killPreparationTimer();
        drawPlots();
    }
}

void MainWindow::keyReleaseEvent(QKeyEvent *event) {
//    if (event->key() == Qt::Key_R)
//        startPresentation();
}

void MainWindow::on_pushButton_start_clicked() {
    killPreparationTimer();

    startSimulation();
    ui->pushButton_start->setEnabled(false);
    ui->pushButton_pause->setEnabled(true);
    ui->pushButton_stop->setEnabled(true);
    _plotsWidget->setAnimationEnabled(false);
    ui->groupBox_time->setEnabled(false);

    ui->comboBox_mode->setEnabled(false);
    ui->comboBox_submode->setEnabled(false);
    ui->comboBox_qoscilator->setEnabled(false);
}

void MainWindow::on_pushButton_pause_clicked() {
    pauseSimulation();
    ui->pushButton_start->setEnabled(true);
    ui->pushButton_pause->setEnabled(false);
    ui->pushButton_stop->setEnabled(true);
    _plotsWidget->setAnimationEnabled(true);
    ui->groupBox_time->setEnabled(false);
}

void MainWindow::on_pushButton_stop_clicked() {
    stopSimulation();
    ui->pushButton_start->setEnabled(true);
    ui->pushButton_pause->setEnabled(false);
    ui->pushButton_stop->setEnabled(false);
    _plotsWidget->setAnimationEnabled(true);
    ui->groupBox_time->setEnabled(true);

    ui->comboBox_mode->setEnabled(true);
    ui->comboBox_submode->setEnabled(true);
    ui->comboBox_qoscilator->setEnabled(true);
}

void MainWindow::on_checkBox_smooth_toggled(bool checked) {
    _plotsWidget->setSmoothEnabled(checked);
}

void MainWindow::on_checkBox_grid_toggled(bool checked) {
    _plotsWidget->setWireframeEnabled(checked);
}

void MainWindow::on_horizontalSlider_time_valueChanged(int value) {
    _plotsWidget->setAnimationEnabled(false);
    _funcBuilder.setT(ui->spinBox_maxTime->value()*double(value)/1000);
    drawPlots();
//    _plotsWidget->setAnimationEnabled(true);
}

void MainWindow::on_comboBox_mode_currentIndexChanged(int index) {
    //setMode(Mode(index));
    ui->comboBox_submode->clear();
    switch (index) {
    case 0:
        ui->comboBox_submode->addItems({"Classic", "Quantum"});
        break;
    case 1:
        ui->comboBox_submode->addItems({"Classic", "Quantum static",
                                       "Quantum dynamic"});
        break;
    case 2:
        ui->comboBox_submode->addItems({"Classic", "Quantum"});
        break;
    }
    //updateMode();
}

void MainWindow::on_comboBox_submode_currentIndexChanged(int index) {
    if (ui->comboBox_mode->currentIndex() == 1 && index == 1)
        ui->comboBox_qoscilator->show();
    else
        ui->comboBox_qoscilator->hide();
}

void MainWindow::on_comboBox_style_currentIndexChanged(int index) {
    setStyle(Style(index));
}

void MainWindow::prepareToSimulation() {
    _funcBuilder.setT(0);
    configure();
    _prepared = true;
}

bool MainWindow::isConfigurationChanged() const {
    return !(_conf.rangeMinX == ui->doubleSpinBox_range_x_Min->value()
            && _conf.rangeMaxX == ui->doubleSpinBox_range_x_Max->value()
            && _conf.rangeMinP == ui->doubleSpinBox_range_p_Min->value()
            && _conf.rangeMaxP == ui->doubleSpinBox_range_p_Max->value()
            && _conf.step3D == ui->doubleSpinBox_step3d->value()
            && _conf.step2D == ui->doubleSpinBox_step2d->value()
            && _conf.initial == ui->groupBox_input->getValues());
}

void MainWindow::rewriteConfiguration() {
    _conf.rangeMinX = ui->doubleSpinBox_range_x_Min->value();
    _conf.rangeMaxX = ui->doubleSpinBox_range_x_Max->value();
    _conf.rangeMinP = ui->doubleSpinBox_range_p_Min->value();
    _conf.rangeMaxP = ui->doubleSpinBox_range_p_Max->value();
    _conf.step3D = ui->doubleSpinBox_step3d->value();
    _conf.step2D = ui->doubleSpinBox_step2d->value();
    _conf.initial = ui->groupBox_input->getValues();
}

void MainWindow::drawPlots() {
    if (!_prepared)
        prepareToSimulation();

    _plotsWidget->set3DFunction(std::move(_funcBuilder.build3DFunction()));
    _plotsWidget->setWxFunction(std::move(_funcBuilder.buildWxFunction()));
    _plotsWidget->setWpFunction(std::move(_funcBuilder.buildWpFunction()));
    _plotsWidget->setXAverageFunction(std::move(_funcBuilder.buildXAverageFunction()));
    _plotsWidget->setPAverageFunction(std::move(_funcBuilder.buildPAverageFunction()));
}

void MainWindow::configure() {
    if (_modeChanged || isConfigurationChanged()) {
        rewriteConfiguration();
        _modeChanged = false;

        _funcBuilder.setRangeX(_conf.rangeMinX, _conf.rangeMaxX);
        _funcBuilder.setRangeP(_conf.rangeMinP, _conf.rangeMaxP);
        _funcBuilder.setRangeAverage(-10, 10);

        _funcBuilder.setInitialConditions(_conf.initial);

        _funcBuilder.setStep3D(_conf.step3D);
        _funcBuilder.setStep2D(_conf.step2D);

        _plotsWidget->setRangeWx_x(_conf.rangeMinX, _conf.rangeMaxX);
        _plotsWidget->setRangeWp_p(_conf.rangeMinP, _conf.rangeMaxP);

        _plotsWidget->setRangeXAverage_x(_conf.rangeMinX, _conf.rangeMaxX);
        _plotsWidget->setRangeXAverage_t(-10, 10);
        _plotsWidget->setRangePAverage_p(_conf.rangeMinP, _conf.rangeMaxP);
        _plotsWidget->setRangePAverage_t(-10, 10);

        _plotsWidget->setRangeW3D_x(_conf.rangeMinX, _conf.rangeMaxX);
        _plotsWidget->setRangeW3D_p(_conf.rangeMinP, _conf.rangeMaxP);

        _plotsWidget->setRangeW3D_W(_funcBuilder.getMinValueW3D(),
                                    _funcBuilder.getMaxValueW3D());
        _plotsWidget->setRangeWx_W(_funcBuilder.getMinValueWx(),
                                   _funcBuilder.getMaxValueWx());
        _plotsWidget->setRangeWp_W(_funcBuilder.getMinValueWp(),
                                   _funcBuilder.getMaxValueWp());
        _plotsWidget->setRangeXAverage_x(_funcBuilder.getMinValueAverageX(),
                                         _funcBuilder.getMaxValueAverageX());
        _plotsWidget->setRangePAverage_p(_funcBuilder.getMinValueAverageP(),
                                         _funcBuilder.getMaxValueAverageP());

        if (_paused || !_simulating)
            drawPlots();
    }
}

void MainWindow::updateMode() {
    switch(ui->comboBox_mode->currentIndex()) {
    // particle
    case 0:
        switch(ui->comboBox_submode->currentIndex()) {
        case 0:
            setMode(Mode::cParticle);
            break;
        case 1:
            setMode(Mode::qParticle);
            break;
        }
        break;
    // oscillator
    case 1:
        switch(ui->comboBox_submode->currentIndex()) {
        case 0:
            setMode(Mode::cOscillator);
            break;
        case 1:
            switch(ui->comboBox_qoscilator->currentIndex()) {
            case 0:
                setMode(Mode::qOscillator0);
                break;
            case 1:
                setMode(Mode::qOscillator1);
                break;
            case 2:
                setMode(Mode::qOscillator2);
                break;
            case 3:
                setMode(Mode::qOscillator3);
                break;
            case 4:
                setMode(Mode::qOscillator4);
                break;
            }
            break;
        case 2:
            setMode(Mode::qOscDynamic);
            break;
        }
        break;
    // potentioal well
    case 2:
        switch(ui->comboBox_submode->currentIndex()) {
        case 0:
            setMode(Mode::cWell);
            break;
        case 1:
            setMode(Mode::qWell);
            break;
        }
        break;
    }
}

void MainWindow::startPreparationTimer() {
    killPreparationTimer();
    _timerIdPreparation = startTimer(700);
}

void MainWindow::killPreparationTimer() {
    if (_timerIdPreparation != -1) {
        killTimer(_timerIdPreparation);
        _timerIdPreparation = -1;
    }
}

void MainWindow::on_pushButton_rotate_clicked() {
    if (_timerIdCamera == -1)
        _timerIdCamera = startTimer(20);
    else {
        killTimer(_timerIdCamera);
        _timerIdCamera = -1;
    }
}

void MainWindow::startSimulation() {
    try {
        if (!_paused)
            prepareToSimulation();
        _paused = false;
        _simulating = true;
        if (_timerIdStep == -1)
            _timerIdStep = startTimer(30);
    }
    catch (const std::exception &e) {
        // stopping timer
        on_pushButton_stop_clicked();

        QMessageBox::warning(this, "Error", e.what());
    }
}

void MainWindow::stopSimulation() {
    _paused = false;
    _simulating = false;
    if (_timerIdStep != -1) {
        killTimer(_timerIdStep);
        _timerIdStep = -1;
    }
}

void MainWindow::pauseSimulation() {
    _paused = true;
    if (_timerIdStep != -1) {
        killTimer(_timerIdStep);
        _timerIdStep = -1;
    }
}

void MainWindow::startPresentation() {
    _plotsWidget->setAnimationEnabled(false);
    //_plotsWidget->rotateCamera(-100);
    ui->checkBox_smooth->setChecked(true);
    if (_timerIdPresentation == -1) {
        _presentationStep = 0;
        //on_pushButton_rotate_clicked();
        presentationStep();
        _timerIdPresentation = startTimer(6500);
    }
}

void MainWindow::presentationStep() {
    if (_presentationStep < 7) {
        stopSimulation();
        ui->doubleSpinBox_range_x_Max->setValue(4);
        ui->doubleSpinBox_range_x_Min->setValue(-4);
        ui->doubleSpinBox_range_p_Max->setValue(4);
        ui->doubleSpinBox_range_p_Min->setValue(-4);
        ui->comboBox_mode->setCurrentIndex(7);
        switch (_presentationStep) {
        case 0:
            ui->groupBox_input->setValues({1, 1, 1, 1, 1, 1, 1});
            break;
        case 1:
            ui->groupBox_input->setValues({1, 1, 1, 1, 1, 1, 0});
            break;
        case 2:
            ui->groupBox_input->setValues({1, 1, 1, 0, 1, 1, 0});
            break;
        case 3:
            ui->groupBox_input->setValues({1, 1, 1, 0.4, 0.3, 0.3, 0.7});
            break;
        case 4:
            ui->groupBox_input->setValues({1, 1, 1, 0.3, 0, 0.4, 0.1});
            break;
        case 5:
            ui->groupBox_input->setValues({1, 1, 1, 0.3, 0.2, 0.4, 0.1});
            break;
        case 6:
            ui->groupBox_input->setValues({1, 1, 1, 0.8, 0.4, 0.6, 0.3});
            break;
        }

        startSimulation();
    }
    else {
        killTimer(_timerIdPresentation);
        _timerIdPresentation = -1;
        //on_pushButton_rotate_clicked();
        stopSimulation();
    }
//    if (_presentationStep < 7) {
//        stopSimulation();

//        ui->comboBox_mode->setCurrentIndex(1);
//        switch (_presentationStep) {
//        case 0:
//            ui->groupBox_input->setValues({1, 1, 2, 1, 1, 1, 0});
//            break;
//        case 1:
//            ui->groupBox_input->setValues({1, 1, 2, 1, 1, 1, 0.3});
//            break;
//        case 2:
//            ui->groupBox_input->setValues({1, 1, 2, 1, 1, 1, 0.7});
//            break;
//        case 3:
//            ui->groupBox_input->setValues({1, 1, 2, 1, 1, 1, 0.95});
//            break;
//        case 4:
//            ui->groupBox_input->setValues({2, 1, 2, 1, 1, 1, 0});
//            break;
//        case 5:
//            ui->groupBox_input->setValues({0.5, 1, 2, 1, 1, 1, 0});
//            break;
//        case 6:
//            ui->groupBox_input->setValues({0.5, 1, 2, 1, 1, 1, 0.5});
//            break;
//        }

//        startSimulation();
//    }
//    else {
//        killTimer(_timerIdPresentation);
//        _timerIdPresentation = -1;
//        //on_pushButton_rotate_clicked();
//        stopSimulation();
//    }
//    if (_presentationStep < 5) {
//        on_pushButton_rotate_clicked();
//        stopSimulation();
//        ui->doubleSpinBox_rangeMax->setValue(4);
//        ui->doubleSpinBox_rangeMin->setValue(-4);

//        ui->comboBox_mode->setCurrentIndex(2 + _presentationStep);

//        startSimulation();
//        on_pushButton_rotate_clicked();
//    }
//    else {
//        killTimer(_timerIdPresentation);
//        _timerIdPresentation = -1;
//        on_pushButton_rotate_clicked();
//        stopSimulation();
//    }
//    if (_presentationStep < 10) {
//        on_pushButton_rotate_clicked();
//        stopSimulation();
//        switch (_presentationStep) {
//        case 0: case 1: case 2: case 3:
//            ui->comboBox_mode->setCurrentIndex(9);
//            ui->groupBox_input->setValues({7, _presentationStep + 1});
//            break;
//        case 4: case 5: case 6: case 7:
//            ui->groupBox_input->setValues({7, (_presentationStep - 3)*5});
//            break;
//        case 8:
//            ui->groupBox_input->setValues({7, 40});
//            break;
//        case 9:
//            ui->groupBox_input->setValues({7, 60});
//            break;
//        }
//        startSimulation();
//        on_pushButton_rotate_clicked();
//    }
//    else {
//        killTimer(_timerIdPresentation);
//        _timerIdPresentation = -1;
//        on_pushButton_rotate_clicked();
//        stopSimulation();
//    }
//    if (_presentationStep < 18) {
//        on_pushButton_rotate_clicked();
//        stopSimulation();
//        if (_presentationStep >= 0
//                && _presentationStep <= 7) {
//            ui->comboBox_mode->setCurrentIndex(_presentationStep);
//        }
//        else {
//            switch (_presentationStep) {
//            case 8: case 9: case 10: case 11:
//                ui->comboBox_mode->setCurrentIndex(9);
//                ui->groupBox_input->setValues({1, _presentationStep - 7});
//                ui->doubleSpinBox_rangeMax->setValue(4);
//                ui->doubleSpinBox_rangeMin->setValue(-4);
//                break;
//            case 12: case 13: case 14: case 15:
//                ui->groupBox_input->setValues({1, (_presentationStep - 11)*5});
//                break;
//            case 16:
//                ui->groupBox_input->setValues({1, 40});
//                break;
//            case 17:
//                ui->groupBox_input->setValues({1, 60});
//                break;
//            }
//        }

//        startSimulation();
//        on_pushButton_rotate_clicked();
//    }
//    else {
//        killTimer(_timerIdPresentation);
//        _timerIdPresentation = -1;
//        on_pushButton_rotate_clicked();
//        stopSimulation();
//    }

    ++_presentationStep;
}

void MainWindow::setMode(Mode mode) {
    if (_conf.mode == mode)
        return;

    _conf.mode = mode;
    _modeChanged = true;
    _prepared = false;
    switch (mode) {
    // particle
    case Mode::cParticle:
        ui->groupBox_input->setFields({"m", "x<sub>c</sub>", "p<sub>c</sub>",
                                       "dx", "dp", "r"});
        ui->groupBox_input->setValues({1, 1, 1, 1, 1, 0});
        _conf.statical = false;
        break;

    // quantum particle
    case Mode::qParticle:
        ui->groupBox_input->setFields({"m", "x<sub>c</sub>",
                                       "dx", "h", "k<sub>c</sub>"});
        ui->groupBox_input->setValues({1, 1, 1, 1, 1});
        _conf.statical = false;
        break;

    // oscillator
    case Mode::cOscillator:
        ui->groupBox_input->setFields({"m", "w", "x<sub>c</sub>",
                                       "p<sub>c</sub>", "dx", "dp", "r"});
        ui->groupBox_input->setValues({0.5, 1, 3, 1, 1, 1, 0});
        _conf.statical = false;
        break;

    case Mode::qOscillator0:
        ui->groupBox_input->setFields({"q<sub>0</sub>", "p<sub>0</sub>"});
        ui->groupBox_input->setValues({1, 1});
        _conf.statical = true;
        break;

    case Mode::qOscillator1:
        ui->groupBox_input->setFields({"q<sub>0</sub>", "p<sub>0</sub>"});
        ui->groupBox_input->setValues({1, 1});
        _conf.statical = true;
        break;

    case Mode::qOscillator2:
        ui->groupBox_input->setFields({"q<sub>0</sub>", "p<sub>0</sub>"});
        ui->groupBox_input->setValues({1, 1});
        _conf.statical = true;
        break;

    case Mode::qOscillator3:
        ui->groupBox_input->setFields({"q<sub>0</sub>", "p<sub>0</sub>"});
        ui->groupBox_input->setValues({1, 1});
        _conf.statical = true;
        break;

    case Mode::qOscillator4:
        ui->groupBox_input->setFields({"q<sub>0</sub>", "p<sub>0</sub>"});
        ui->groupBox_input->setValues({1, 1});
        _conf.statical = true;
        break;

    case Mode::qOscDynamic:
        ui->groupBox_input->setFields({"q<sub>0</sub>", "p<sub>0</sub>", "w",
                                       "C<sub>0</sub>", "C<sub>1</sub>",
                                       "C<sub>2</sub>", "C<sub>3</sub>"});
        ui->groupBox_input->setValues({1, 1, 1, 1, 1, 1, 1});
        _conf.statical = false;
        break;

    // potential well
    case Mode::cWell:
        ui->groupBox_input->setFields({"m", "l", "x<sub>c</sub>",
                                       "p<sub>c</sub>", "dx", "dp"});
        ui->groupBox_input->setValues({1, 1, 1, 1, 1, 1});
        _conf.statical = false;
        break;

    case Mode::qWell:
        ui->groupBox_input->setFields({"a", "n"});
        ui->groupBox_input->setValues({7, 1});
        ui->groupBox_input->setInteger({false, true});
        _conf.statical = true;
        break;

    default:
        ui->groupBox_input->setFields({"mode", "error"});
        QMessageBox::warning(this, "Error", "Mode id error");
        break;
    }
    _funcBuilder.setMode(mode);
    _plotsWidget->clear();

    ui->groupBox_time->setEnabled(!_conf.statical);
    ui->pushButton_start->setEnabled(!_conf.statical);

    startPreparationTimer();
}

void MainWindow::setStyle(MainWindow::Style style) {
    QPalette pal;
    switch (style) {
    case Style_Light:
        pal = QApplication::palette();
        _plotsWidget->setStyle(PlotsWidget::Style_Light);
        break;
    case Style_Dark:
        pal = window()->palette();
        pal.setColor(QPalette::Window, QRgb(0x40434a));
        pal.setColor(QPalette::WindowText, QRgb(0xd6d6d6));
        pal.setColor(QPalette::Background, QRgb(0x40434a));
        _plotsWidget->setStyle(PlotsWidget::Style_Dark);
        break;
    }
    this->setPalette(pal);
    ui->groupBox_plot->setPalette(pal);
    ui->groupBox_view->setPalette(pal);
    ui->groupBox_input->setPalette(pal);
    ui->groupBox_time->setPalette(pal);
    ui->tabWidget->setPalette(pal);
    ui->tabWidget->tabBar()->setPalette(pal);
    ui->tab_simulation->setPalette(pal);
    ui->tab_settings->setPalette(pal);
    _plotsWidget->setPalette(pal);
}
