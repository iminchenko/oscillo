// Copyright 2017 <Ilia Minchenko>
#pragma once

#include <QMainWindow>
#include <QtCharts/QChartGlobal>
#include <QtDataVisualization/Q3DSurface>
#include <vector>
#include <list>
#include <memory>

//#include "oscillator.h"
#include "functionbuilder.h"
#include "plotswidget.h"
#include "modes.h"

QT_CHARTS_USE_NAMESPACE

using namespace QtDataVisualization;

//class PlotsHandler;

namespace Ui {
class MainWindow;
}

struct configuration {
    double rangeMinX;
    double rangeMaxX;
    double rangeMinP;
    double rangeMaxP;
    double step3D;
    double step2D;
    std::list<double> initial;
    Mode mode;
    // mode is static
    bool statical;
};

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

protected:
    virtual void timerEvent(QTimerEvent *event);
    virtual void keyReleaseEvent(QKeyEvent *event);

private slots:
    void on_pushButton_start_clicked();

    void on_pushButton_stop_clicked();

    void on_checkBox_smooth_toggled(bool checked);

    void on_checkBox_grid_toggled(bool checked);

    void on_pushButton_pause_clicked();

    void on_horizontalSlider_time_valueChanged(int value);

    void on_comboBox_mode_currentIndexChanged(int index);

    void on_comboBox_submode_currentIndexChanged(int index);

    void on_comboBox_style_currentIndexChanged(int index);

    void on_pushButton_rotate_clicked();

    // set ranges and steps
    void configure();
    // setting the same mode as in comboboxes
    void updateMode();

    void startPreparationTimer();
    void killPreparationTimer();

private:
    enum Style {
        Style_Light = 0,
        Style_Dark
    };

    // before every start
    void prepareToSimulation();
    bool isConfigurationChanged() const;
    void rewriteConfiguration();
    // draw single time
    void drawPlots();

    // simulation control
    void startSimulation();
    void stopSimulation();
    void pauseSimulation();

    // presentation control
    void startPresentation();
    void presentationStep();

    // mode switching
    void setMode(Mode mode);
    // style switching
    void setStyle(Style style);

    Ui::MainWindow *ui;
    int _timerIdStep;
    int _timerIdCamera;
    int _timerIdPresentation;
    int _timerIdPreparation;

    bool _paused;
    bool _simulating;
    // prepared to simulation
    bool _prepared;
    // for forced reconfiguration after mode changing
    bool _modeChanged;

    int _presentationStep;

    FunctionBuilder _funcBuilder;

    PlotsWidget *_plotsWidget;

    // current plot configuration
    configuration _conf;
};
