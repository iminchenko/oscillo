// Copyright 2017 <Ilia Minchenko>
#include "mathdefines.h"

double sqr(double x) {
    return x*x;
}
