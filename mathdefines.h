// Copyright 2017 <Ilia Minchenko>
#pragma once

namespace MathConst {
constexpr double Pi = 3.14159265359;
constexpr double Pi2 = Pi*2;
constexpr double Pi_2 = Pi/2;
constexpr double Pi_4 = Pi/4;
constexpr double E = 2.71828182845;
}

double sqr(double x);
