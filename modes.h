// Copyright 2017 <Ilia Minchenko>
#pragma once

enum Mode {
    cParticle = 0,
    qParticle,
    cOscillator,
    qOscillator0,
    qOscillator1,
    qOscillator2,
    qOscillator3,
    qOscillator4,
    qOscDynamic,
    cWell,
    qWell
};
