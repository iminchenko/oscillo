// Copyright 2017 <Ilia Minchenko>
#include "plotswidget.h"
#include <QLabel>

#define CHARTHEIGHT 700

using std::vector;

QChart *createLineChart(const QString &title = "") {
    QChart *chart = new QChart();

    chart->setTitle(title);

    chart->addSeries(new QLineSeries(chart));
    chart->createDefaultAxes();
    chart->legend()->hide();

    return chart;
}

void vectorToSeries(QLineSeries *series, const vector<QPointF> &func) {
    QList<QPointF> list;
    for (const auto &iter : func) {
        list.append(iter);
    }

    series->replace(list);
}

PlotsWidget::PlotsWidget(QWidget *parent)
    :QScrollArea(parent) {
    _scrollAreaWidgetContents = new QWidget();
    _scrollAreaWidgetContents->setGeometry(0, 0, this->width(), CHARTHEIGHT + this->width()*0.7);
    this->setWidget(_scrollAreaWidgetContents);

    _mainLayout = new QVBoxLayout(_scrollAreaWidgetContents);

    initializePlots();

    configureSurface();
    setStyle(Style::Style_Light);
}

void PlotsWidget::setRangeWx_x(float min, float max) {
    if (min == max) {
        max += 0.2;
        min -= 0.2;
    }

    _chartWx->axisX()->setRange(min, max);
}

void PlotsWidget::setRangeWx_W(float min, float max) {
    if (min == max) {
        max += 0.2;
        min -= 0.2;
    }

    min -= 0.2*fabs(min);
    max += 0.2*fabs(max);
    _chartWx->axisY()->setRange(min, max);
}

void PlotsWidget::setRangeWp_p(float min, float max) {
    if (min == max) {
        max += 0.2;
        min -= 0.2;
    }

    _chartWp->axisX()->setRange(min, max);
}

void PlotsWidget::setRangeWp_W(float min, float max) {
    if (min == max) {
        max += 0.2;
        min -= 0.2;
    }

    min -= 0.2*fabs(min);
    max += 0.2*fabs(max);
    _chartWp->axisY()->setRange(min, max);
}

void PlotsWidget::setRangeXAverage_x(float min, float max) {
    if (min == max) {
        max += 0.2;
        min -= 0.2;
    }

    min -= 0.2*fabs(min);
    max += 0.2*fabs(max);
    _chartXAverage->axisY()->setRange(min, max);
}

void PlotsWidget::setRangeXAverage_t(float min, float max) {
    if (min == max) {
        max += 0.2;
        min -= 0.2;
    }

    _chartXAverage->axisX()->setRange(min, max);
}

void PlotsWidget::setRangePAverage_p(float min, float max) {
    if (min == max) {
        max += 0.2;
        min -= 0.2;
    }

    min -= 0.2*fabs(min);
    max += 0.2*fabs(max);
    _chartPAverage->axisY()->setRange(min, max);
}

void PlotsWidget::setRangePAverage_t(float min, float max) {
    if (min == max) {
        max += 0.2;
        min -= 0.2;
    }

    _chartPAverage->axisX()->setRange(min, max);
}

void PlotsWidget::setRangeW3D_W(float min, float max) {
    if (min == max) {
        max += 0.2;
        min -= 0.2;
    }

    min -= 0.2*fabs(min);
    max += 0.2*fabs(max);
    _surfaceW->axisY()->setRange(min, max);
}

void PlotsWidget::setRangeW3D_x(float min, float max){
    if (min == max) {
        max += 0.2;
        min -= 0.2;
    }

    _surfaceW->axisX()->setRange(min, max);
}

void PlotsWidget::setRangeW3D_p(float min, float max) {
    if (min == max) {
        max += 0.2;
        min -= 0.2;
    }

    _surfaceW->axisZ()->setRange(min, max);
}

void PlotsWidget::setAnimationEnabled(bool enabled) {
    if (enabled) {
        _chartWx->setAnimationOptions(QChart::SeriesAnimations);
        _chartWp->setAnimationOptions(QChart::SeriesAnimations);
        _chartXAverage->setAnimationOptions(QChart::SeriesAnimations);
        _chartPAverage->setAnimationOptions(QChart::SeriesAnimations);
    }
    else {
        _chartWx->setAnimationOptions(QChart::NoAnimation);
        _chartWp->setAnimationOptions(QChart::NoAnimation);
        _chartXAverage->setAnimationOptions(QChart::NoAnimation);
        _chartPAverage->setAnimationOptions(QChart::NoAnimation);
    }
}

void PlotsWidget::setStyle(PlotsWidget::Style style) {
    _theme->setWindowColor(this->palette().window().color());
    QLinearGradient gr;
    switch (style) {
    case PlotsWidget::Style_Light:
        // surface
        _surfaceW->setReflection(false);

        _theme->setType(Q3DTheme::ThemePrimaryColors);
        _theme->setAmbientLightStrength(0.8f);
        _theme->setLightStrength(0.8f);
        gr.setColorAt(0.0, Qt::blue);
        gr.setColorAt(0.33, QColor(Qt::green).darker(100));
        gr.setColorAt(0.66, Qt::yellow);
        gr.setColorAt(1, Qt::red);

        _theme->setBaseGradients({gr});
        _theme->setColorStyle(Q3DTheme::ColorStyleRangeGradient);

        // line plots
        _chartWx->setTheme(QChart::ChartThemeLight);
        _chartWp->setTheme(QChart::ChartThemeLight);
        _chartXAverage->setTheme(QChart::ChartThemeLight);
        _chartPAverage->setTheme(QChart::ChartThemeLight);
        break;

    case PlotsWidget::Style_Dark:
        // surface
        _surfaceW->setReflection(false);

        _theme->setType(Q3DTheme::ThemeArmyBlue);

        _theme->setAmbientLightStrength(0.8f);
        _theme->setLightStrength(0.8f);

        QLinearGradient gr;
        gr.setColorAt(0.0, QColor(Qt::darkBlue).lighter(100));
        gr.setColorAt(0.33, Qt::darkCyan);
        gr.setColorAt(0.57, Qt::green);
        gr.setColorAt(1.0, Qt::cyan);

        _theme->setBaseGradients({gr});
        _theme->setColorStyle(Q3DTheme::ColorStyleRangeGradient);
        _theme->setBackgroundColor(QColor(0, 20, 40));

        // line plots
        _chartWx->setTheme(QChart::ChartThemeBlueCerulean);
        _chartWp->setTheme(QChart::ChartThemeBlueCerulean);
        _chartXAverage->setTheme(QChart::ChartThemeBlueCerulean);
        _chartPAverage->setTheme(QChart::ChartThemeBlueCerulean);
        break;
    }
//    _theme->setBackgroundEnabled(false);
//    _theme->setGridEnabled(false);
//    _theme->setLabelBorderEnabled(false);
//    _theme->setLabelBorderEnabled(false);
}

void PlotsWidget::setPalette(const QPalette &pal) {
    QScrollArea::setPalette(pal);

    _theme->setWindowColor(this->palette().window().color());
}

void PlotsWidget::setSmoothEnabled(bool enabled) {
    _3dSeriesFunc->setFlatShadingEnabled(!enabled);
    _3dSeriesFunc->setMeshSmooth(enabled);
}

void PlotsWidget::setWireframeEnabled(bool enabled) {
    if (enabled)
        _3dSeriesFunc->setDrawMode(QSurface3DSeries::DrawSurfaceAndWireframe);
    else
        _3dSeriesFunc->setDrawMode(QSurface3DSeries::DrawSurface);
}

void PlotsWidget::set3DFunction(const matrix &func) {
    QSurfaceDataArray *dataArray = new QSurfaceDataArray;
    if (!func.empty()) {
        dataArray->reserve(func[0].size());
        for (size_t i = 0 ; i < func[0].size() ; i++) {
            QSurfaceDataRow *newRow = new QSurfaceDataRow(func.size());
            // Keep values within range bounds, since just adding step can cause minor drift due
            // to the rounding errors.
            for (size_t j = 0; j < func.size(); ++j) {
                (*newRow)[j].setPosition(func[j][i]);
            }
            *dataArray << newRow;
        }
    }
    _3dProxyFunc->resetArray(dataArray);
}

void PlotsWidget::setWxFunction(const std::vector<QPointF> &func) {
    vectorToSeries(dynamic_cast<QLineSeries *>(_chartWx->series().first()), func);
}

void PlotsWidget::setWpFunction(const std::vector<QPointF> &func) {
    vectorToSeries(dynamic_cast<QLineSeries *>(_chartWp->series().first()), func);
}

void PlotsWidget::setXAverageFunction(const std::vector<QPointF> &func) {
    vectorToSeries(dynamic_cast<QLineSeries *>(_chartXAverage->series().first()), func);
}

void PlotsWidget::setPAverageFunction(const std::vector<QPointF> &func) {
    vectorToSeries(dynamic_cast<QLineSeries *>(_chartPAverage->series().first()), func);
}

void PlotsWidget::clear() {
    _3dProxyFunc->resetArray(NULL);
    dynamic_cast<QLineSeries *>(_chartWx->series().first())->replace(QList<QPointF>());
    dynamic_cast<QLineSeries *>(_chartWp->series().first())->replace(QList<QPointF>());
    dynamic_cast<QLineSeries *>(_chartXAverage->series().first())->replace(QList<QPointF>());
    dynamic_cast<QLineSeries *>(_chartPAverage->series().first())->replace(QList<QPointF>());
}

void PlotsWidget::rotateCamera(double angle) {
    auto cam = _surfaceW->scene()->activeCamera();
    cam->setXRotation(cam->xRotation() + angle);
    cam->setYRotation(sin(cam->xRotation()*M_PI/180)*15 + 16);
}

void PlotsWidget::resizeEvent(QResizeEvent *e) {
    QScrollArea::resizeEvent(e);
    int width = this->width() - 25;
    int height = 0.8*width + std::max(_chartViewWx->minimumHeight()*2 + 30, width/2);
    _scrollAreaWidgetContents->setGeometry(0, 0, width, height);
    _surfaceContainer->setMinimumWidth(width);
    _surfaceContainer->setMinimumHeight(width * 0.8);

    this->setMinimumWidth(_chartViewWx->minimumWidth()*2 + 40);
}

void PlotsWidget::initializePlots() {
    // surface
    _surfaceW = new Q3DSurface(Q_NULLPTR, Q_NULLPTR);

    _surfaceContainer = QWidget::createWindowContainer(_surfaceW);

    _surfaceContainer->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
    _surfaceContainer->setMinimumHeight(600);
    _scrollAreaWidgetContents->layout()->addWidget(_surfaceContainer);

    _3dProxyFunc = new QSurfaceDataProxy();
    _3dSeriesFunc = new QSurface3DSeries(_3dProxyFunc);
    _3dSeriesFunc->setDrawMode(QSurface3DSeries::DrawSurface);

    _surfaceW->addSeries(_3dSeriesFunc);

    // line plots
    _chartWx = createLineChart("W<sub>x</sub>");
    _chartWp = createLineChart("W<sub>p</sub>");
    _chartXAverage = createLineChart("&lt;X&gt;");
    _chartPAverage = createLineChart("&lt;P&gt;");

    _chartViewWx = new QChartView(_chartWx, _scrollAreaWidgetContents);
    _chartViewWp = new QChartView(_chartWp, _scrollAreaWidgetContents);
    _chartViewXAverage = new QChartView(_chartXAverage, _scrollAreaWidgetContents);
    _chartViewPAverage = new QChartView(_chartPAverage, _scrollAreaWidgetContents);

    QHBoxLayout *hLayout = new QHBoxLayout();
    _mainLayout->addLayout(hLayout);
    hLayout->addWidget(_chartViewWx);
    hLayout->addWidget(_chartViewWp);
    hLayout = new QHBoxLayout();
    _mainLayout->addLayout(hLayout);
    hLayout->addWidget(_chartViewXAverage);
    hLayout->addWidget(_chartViewPAverage);

    _chartViewWx->setRenderHint(QPainter::Antialiasing, true);
    _chartViewWp->setRenderHint(QPainter::Antialiasing, true);
    _chartViewXAverage->setRenderHint(QPainter::Antialiasing, true);
    _chartViewPAverage->setRenderHint(QPainter::Antialiasing, true);

    this->setMinimumWidth(_chartViewWx->minimumWidth()*2 + 40);
}

void PlotsWidget::configureSurface() {
    // style
    _theme = new Q3DTheme();
    _surfaceW->setActiveTheme(_theme);
    _surfaceW->setOrthoProjection(true);
    // axis
    _surfaceW->axisX()->setLabelFormat("%.2f");
    _surfaceW->axisZ()->setLabelFormat("%.2f");
    _surfaceW->axisY()->setRange(0.0f, 2.0f);
    _surfaceW->axisX()->setLabelAutoRotation(90);
    _surfaceW->axisY()->setLabelAutoRotation(90);
    _surfaceW->axisZ()->setLabelAutoRotation(90);

    _surfaceW->axisX()->setTitle(QStringLiteral("x"));
    _surfaceW->axisZ()->setTitle(QStringLiteral("p"));
    _surfaceW->axisY()->setTitle(QStringLiteral("W"));
    _surfaceW->axisX()->setTitleVisible(true);
    _surfaceW->axisY()->setTitleVisible(true);
    _surfaceW->axisZ()->setTitleVisible(true);
    _surfaceW->axisX()->setTitleFixed(false);
    _surfaceW->axisY()->setTitleFixed(false);
    _surfaceW->axisZ()->setTitleFixed(false);


    _surfaceW->setShadowQuality(Q3DSurface::ShadowQualityNone);

    _surfaceW->scene()->activeCamera()->setCameraPreset(Q3DCamera::CameraPresetIsometricLeft);
}
