// Copyright 2017 <Ilia Minchenko>
#pragma once

#include <QScrollArea>
#include <QObject>
#include <QtCharts/QChartGlobal>
#include <QtCharts/QLineSeries>
#include <QChartView>
#include <QChart>
#include <QtDataVisualization/Q3DSurface>
#include <QVector3D>
#include <QPointF>
#include <QSplitter>
#include <QVBoxLayout>
#include <vector>
#include <memory>

QT_CHARTS_USE_NAMESPACE

using namespace QtDataVisualization;

class PlotsWidget : public QScrollArea {
    Q_OBJECT
    using matrix = std::vector<std::vector<QVector3D> >;
public:
    // styles
    enum Style {
        Style_Light = 0,
        Style_Dark
    };

    PlotsWidget(QWidget *parent = Q_NULLPTR);

    void setRangeWx_x(float min, float max);
    void setRangeWx_W(float min, float max);
    void setRangeWp_p(float min, float max);
    void setRangeWp_W(float min, float max);
    void setRangeXAverage_x(float min, float max);
    void setRangeXAverage_t(float min, float max);
    void setRangePAverage_p(float min, float max);
    void setRangePAverage_t(float min, float max);
    void setRangeW3D_W(float min, float max);
    void setRangeW3D_x(float min, float max);
    void setRangeW3D_p(float min, float max);

    void setAnimationEnabled(bool enabled);
    void setStyle(PlotsWidget::Style style);
    virtual void setPalette(const QPalette &pal);

    void setSmoothEnabled(bool enabled);
    void setWireframeEnabled(bool enabled);

    void set3DFunction(const matrix &func);
    void setWxFunction(const std::vector<QPointF> &func);
    void setWpFunction(const std::vector<QPointF> &func);
    void setXAverageFunction(const std::vector<QPointF> &func);
    void setPAverageFunction(const std::vector<QPointF> &func);

    void clear();

    void rotateCamera(double angle);

protected slots:
    void resizeEvent(QResizeEvent *e);

private:
    void initializePlots();
    void configureSurface();

    //QSplitter *_mainSplitter;
    QVBoxLayout *_mainLayout;

    QWidget *_scrollAreaWidgetContents;

    Q3DSurface *_surfaceW;
    QWidget *_surfaceContainer;
    QChart *_chartWx;
    QChart *_chartWp;
    QChart *_chartXAverage;
    QChart *_chartPAverage;
    QChartView *_chartViewWx;
    QChartView *_chartViewWp;
    QChartView *_chartViewXAverage;
    QChartView *_chartViewPAverage;

    Q3DTheme *_theme;

    QSurfaceDataProxy *_3dProxyFunc;
    QSurface3DSeries *_3dSeriesFunc;
};
